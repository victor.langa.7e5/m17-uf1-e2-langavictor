﻿using GameTools;
using MyFirstProgram;
using System;

namespace M17_UF1_E2_LangaVictor
{
    class CharRainGame : GameEngine
    {
        private int matrixWidth;
        private int matrixHeight;
        // matrix is printed in inverse due to MatrixRepresentation.printMatrix()

        private MatrixRepresentation matrix;
        private int[] matrixNextLoop;

        private bool modeChooseColumns;
        private int cursorPosition = 1;

        public CharRainGame() : base()
        { }

        protected override void Start()
        {
            Console.CursorVisible = false;
            Console.SetWindowSize(90, 25);
            GetIfModeColumns();
            GetWindowsSize();

            matrix = new MatrixRepresentation(matrixWidth, matrixHeight);
            matrixNextLoop = new int[matrixHeight];
            Console.SetWindowSize(matrixHeight * 2, matrixWidth + 3);
            Console.SetBufferSize(matrixHeight * 2, matrixWidth + 3);
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nNice! Wait a second");
            Console.ResetColor();
        }

        protected override void Update()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            if (!modeChooseColumns)
            {
                matrix.printMatrix();
                UpdateMatrixNextLoop();
                UpdateMatrix();
            }
            else
            {
                if (Console.KeyAvailable) cursorPosition = UpdateCursor(Console.ReadKey(true));
                matrix.printMatrix();
                PrintCursor(cursorPosition);
                UpdateMatrix();
            }
            Console.ForegroundColor = ConsoleColor.Black; // this is to not show the WriteLine in GameEngine.cs line 109
        }

        protected override void Exit()
        {
            Console.Clear();
            Console.SetWindowSize(90, 25);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void GetIfModeColumns()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\nDo you want to choose the columns where the characters rain? (Y/N)");
            Console.ResetColor();

            modeChooseColumns = CheckYesOrNoReturnBool();
            if (modeChooseColumns)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\n   [TIP]");
                Console.ResetColor();
                Console.Write(" This mode uses Left Arrow and Right Arrow to move the cursor, and Enter to start " +
                    "\n         spawning the char rain in the selected column." +
                    "\n\n         To make it work properly, MANTAIN the keys. Only pressing them will cause to lose " +
                    "\n         some of the inputs." +
                    "\n\n         Mantaining the Enter key for too long can cause the rain to spawn only some rain" +
                    "\n         tears and despawn the rain column after, so git gud at timing ;)\n");
            }
        }

        private void GetWindowsSize()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nDo you want to choose the window size? (Y/N)");
            Console.ResetColor();

            if (CheckYesOrNoReturnBool())
            {
                Console.Write("Introduce the window width (min 20, max 100): ");
                matrixHeight = GetWindowSideLength(20, 100);
                Console.Write("Introduce the window height (min 5, max 50): ");
                matrixWidth = GetWindowSideLength(5, 50);
            }
            else
            {
                matrixWidth = 25;
                matrixHeight = 50;
            }
        }

        private int GetWindowSideLength(int minLength, int maxLength)
        {
            int length;
            bool isValid;

            do
            {
                isValid = int.TryParse(Console.ReadLine(), out length);
                if (length < minLength || length > maxLength) isValid = false;
                if (!isValid)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("   [!]");
                    Console.ResetColor();
                    Console.Write(" Introduce a valid value: ");
                }
            } while (!isValid);

            return length;
        }

        private bool CheckYesOrNoReturnBool()
        {
            bool isValidOption;
            do
            {
                isValidOption = true;
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Y:
                        return true;
                    case ConsoleKey.N:
                        return false;
                    default:
                        isValidOption = false;
                        break;
                }
            } while (!isValidOption);

            return false;
        }

        private void UpdateMatrixNextLoop()
        {
            var rnd = new Random();
            for (int i = 0; i < matrixNextLoop.Length; i++)
            {
                if (matrixNextLoop[i] == 0)
                    if (rnd.Next(0, 100) > 95)
                        matrixNextLoop[i] = rnd.Next(4, 7);
            }
        }

        private void UpdateMatrixNextLoopInColumnMode()
        {
            if (matrixNextLoop[cursorPosition / 2] == 0) matrixNextLoop[cursorPosition / 2] = 1;
            else matrixNextLoop[cursorPosition / 2] = 0;
        }

        private void UpdateMatrix()
        {
            for (int i = matrix.TheMatrix.GetLength(0) - 1; i > 0; i--)
                for (int j = 0; j < matrix.TheMatrix.GetLength(1); j++)
                    matrix.TheMatrix[i, j] = matrix.TheMatrix[i - 1, j];

            var rnd = new Random();
            char[] chars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

            for (int i = 0; i < matrixNextLoop.Length; i++)
            {
                if (matrixNextLoop[i] > 0)
                {
                    matrix.TheMatrix[0, i] = chars[rnd.Next(0, 10)];
                    if (!modeChooseColumns) matrixNextLoop[i]--;
                }
                else matrix.TheMatrix[0, i] = ' ';
            }
        }

        private int UpdateCursor(ConsoleKeyInfo input)
        {
            Console.SetCursorPosition(cursorPosition, matrixWidth);

            switch (input.Key)
            {
                case ConsoleKey.LeftArrow:
                    if (cursorPosition <= 1) return cursorPosition;
                    Console.SetCursorPosition(Console.CursorLeft - 2, Console.CursorTop);
                    return cursorPosition-=2;
                case ConsoleKey.RightArrow:
                    if (cursorPosition >= matrixHeight * 2 - 1) return cursorPosition;
                    return cursorPosition+=2;
                case ConsoleKey.Enter:
                    UpdateMatrixNextLoopInColumnMode();
                    return cursorPosition;
            }

            return cursorPosition;
        }

        private void PrintCursor(int cursorPosition)
        {
            Console.SetCursorPosition(cursorPosition, matrixWidth);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("^");
            Console.SetCursorPosition(Console.CursorLeft-=1, Console.CursorTop+=1);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write(" ");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Green;
        }
    }
}
